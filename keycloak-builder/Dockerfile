# File: /keycloak-builder/Dockerfile
# Project: images
# File Created: 26-11-2023 08:09:02
# Author: Clay Risser <email@clayrisser.com>
# -----
# BitSpur (c) Copyright 2021 - 2023
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ARG KEYCLOAK_VERSION=latest

FROM registry.access.redhat.com/ubi9 AS ubi-micro-build
RUN mkdir -p /mnt/rootfs
RUN dnf module enable --installroot /mnt/rootfs nodejs:20 -y && \
    dnf install --installroot /mnt/rootfs \
    autoconf \
    automake \
    ca-certificates \
    curl \
    gcc \
    gcc-c++ \
    git \
    git-lfs \
    jq \
    libtool \
    make \
    nodejs \
    nodejs-libs \
    npm \
    openssl \
    python \
    --releasever 9 --setopt install_weak_deps=false --nodocs -y && \
    dnf --installroot /mnt/rootfs clean all && \
    rpm --root /mnt/rootfs -e --nodeps setup

FROM quay.io/keycloak/keycloak:${KEYCLOAK_VERSION}
COPY --from=ubi-micro-build /mnt/rootfs /
USER root
RUN npm install -g pnpm yarn corepack && \
    curl -Lo /tmp/maven.tar.gz https://dlcdn.apache.org/maven/maven-3/3.9.8/binaries/apache-maven-3.9.8-bin.tar.gz && \
    cd /tmp && tar -xzvf maven.tar.gz && \
    mv apache-maven-3.9.8 /opt/maven && \
    rm -rf /tmp/maven.tar.gz && \
    ln -s /opt/maven/bin/mvn /usr/local/bin/mvn
USER keycloak
ENV KC_DB=postgres \
    KC_HEALTH_ENABLED=true \
    KC_HTTPS_CERTIFICATE_FILE=/opt/keycloak/conf/tls.crt \
    KC_HTTPS_CERTIFICATE_KEY_FILE=/opt/keycloak/conf/tls.key \
    KC_METRICS_ENABLED=true \
    KC_VAULT=file
WORKDIR /opt/keycloak
RUN openssl req -x509 -newkey rsa:4096 -keyout ${KC_HTTPS_CERTIFICATE_KEY_FILE} \
    -out ${KC_HTTPS_CERTIFICATE_FILE} -days 36500 -nodes -subj "/CN=localhost" && \
    chmod 644 ${KC_HTTPS_CERTIFICATE_FILE} && \
    chmod 600 ${KC_HTTPS_CERTIFICATE_KEY_FILE} && \
    /opt/keycloak/bin/kc.sh build
